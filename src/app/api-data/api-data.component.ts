import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { People } from './people';

@Component({
  selector: 'app-api-data',
  templateUrl: './api-data.component.html',
  styleUrls: ['./api-data.component.scss']
})
export class ApiDataComponent {

      readonly ROOT_URL = 'https://jsonplaceholder.typicode.com/posts';
    people: Observable<People[]>;

  constructor(private http: HttpClient){
    this.people = this.getData();
    
  }
  
  getData(){
    return this.http.get<People[]>(this.ROOT_URL)
  }
  
  

}
