import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { ApiDataComponent } from './api-data/api-data.component';

const routes: Routes = [
    { path: 'user-details', component: UserDetailsComponent },    
    { path: 'api-data', component: ApiDataComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
