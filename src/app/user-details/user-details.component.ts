import { NgModule, Component, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Customer, Service } from './user-details.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  providers: [Service]
})

export class UserDetailsComponent {
    customers: Customer[];
  buttonType: string = 'danger';

    constructor(service: Service) {
        this.customers =  service.getCustomers();
    }
}
